package main

import (
	"bufio"
	"database/sql"
	"fmt"
	"log"
	"os"
	"strings"

	_ "github.com/lib/pq"
	"gopkg.in/cheggaaa/pb.v1"
)

const (
	host     = "localhost"
	port     = 5432
	user     = "postgres"
	password = "root"
	dbname   = "movies_com231"
)

func main() {

	count := 514047
	cont := 0
	bar := pb.StartNew(count)

	psqlInfo := fmt.Sprintf(
		"host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname)
	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	sqlStatement := `INSERT INTO control (movie_id) VALUES($1) RETURNING movie_id`
	var movieID string

	file, err := os.Open("../data.tsv")
	if err != nil {
		log.Fatal(err)
	}

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		typeMovie := strings.Split(scanner.Text(), "\t")
		if strings.Compare(typeMovie[1], "movie") == 0 {
			err = db.QueryRow(sqlStatement, typeMovie[0]).Scan(&movieID)
			if err != nil {
				panic(err)
			}
			// fmt.Println("Inserted ", movieID)
			cont++
			bar.Increment()
		}
	}
	bar.Finish()
}
