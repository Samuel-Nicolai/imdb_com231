package main

import (
	"encoding/json"
	"net/http"
	"time"
)

// URL is the base url to get movie object
const URL string = "http://www.omdbapi.com/?apikey=5e996bc1&r=json&i=" // &i=tt0848228 a key example

var myClient = &http.Client{Timeout: 10 * time.Second} // client http golang

func getJSON(url string, target interface{}) error {
	r, err := myClient.Get(url)
	if err != nil {
		return err
	}
	defer r.Body.Close()

	return json.NewDecoder(r.Body).Decode(target)
}
