package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
)

// Movie is a inteface a object json reference
type Movie struct {
	Title string `json:"Title"`
}

func main() {
	//count := 514047
	cont := 0

	//bar := pb.StartNew(count)
	movie := new(Movie)

	file, err := os.Open("data.tsv")
	if err != nil {
		log.Fatal(err)
	}

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		typeMovie := strings.Split(scanner.Text(), "\t")
		if strings.Compare(typeMovie[1], "movie") == 0 {
			cont++
			//fmt.Printf("%d %s \n", cont, typeMovie[0])
			//bar.Increment()
			getJSON(URL+typeMovie[0], movie)
			fmt.Println(typeMovie[0] + ", " + movie.Title)
		}
	}
	//bar.Finish()
}
